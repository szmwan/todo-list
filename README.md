# 基于Taro多端开发的Todolist小程序

## 简介

这是一个待办事项，输入事项点击添加，点击删除可删除选中的事项

## 主要目录结构

```
myApp/
├── App.js
├── app.json
├── babel.config.js
├── components/
├── cartItem/
│   ├── Cart.js
│   ├── CartItem.js
├── ProductPage/
│   ├── Product.js
│   └── ProductList.js
├── BottomTabNavigator.js
├── package-lock.json
├── package.json
└── README.md

## 版本历史

- v1.0.0 (2024-02-25): 初始版本。
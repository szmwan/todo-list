import "./index.less";

import {
  Button,
  View,
} from "@tarojs/components";
import Taro, { useLoad } from "@tarojs/taro";

export default function Index() {
  const goToStart = () => {
    Taro.navigateTo({
      url: '/pages/todoList/todoList'
    });
  }
  
  useLoad(() => {
    console.log('Page loaded...')
  })
  
  return (
    <View className='index'>
      <Button className='btn' onClick={goToStart}>点击这里开始记事</Button>
    </View>
  )
}

import "./todoList.scss";

import React, {
  useRef,
  useState,
} from "react";

import {
  Button,
  Checkbox,
  Input,
  Text,
  View,
} from "@tarojs/components";
import Taro, { useLoad } from "@tarojs/taro";

export default () => {
  const [visitable,setVisitable] = useState(false)
  const [todoList, setTodoList] = useState([]);
  const $InputRef = useRef();
  useLoad(() => {
    setTodoList(Taro.getStorageSync("todo") || []);
  });
  //定义类型
  const createTodo = (text) => {
    return {
      text,
      checked: false,
    };
  };
  //添加按钮
  const addTodo = () => {
    const targetValue = $InputRef.current?.value;
    if(!targetValue){
        return Taro.showToast({ title: "请先输入~", icon: "none" })
    } else if (todoList.find((todo) => todo.text === targetValue)) {
        return Taro.showToast({ title: "您已添加过该事项~", icon: "none" });
    } else {
        const newTodoList = [...todoList, createTodo(targetValue)];
        setTodoList(newTodoList);
        Taro.setStorageSync("todo", newTodoList); 
        $InputRef.current.value = ''
    }
  };
  //删除按钮
  const handleDeleteClick = () => {
    //分别定义已勾选和未勾选的事项
    const stayTodo = todoList.filter(item => item.checked === false);
    const removeTodo = todoList.filter(item => item.checked === true);
    if(removeTodo.length > 0){
        Taro.showModal({
        title: '确认删除',
        content: '是否确认删除该项？',
        success: function (res) {
            if (res.confirm) {
                setTodoList(stayTodo)
            }
            }
        });
    }else{
        return Taro.showToast({ title: "请选择要删除的待办~", icon: "none" });
    }
    
  }
  const onChecked = (key) => {
    todoList[key].checked = !todoList[key].checked;
    setTodoList([...todoList]);
    Taro.setStorageSync("todo", todoList);
  };
  return (
    <View style={{textAlign:'center'}}>
      <Input className="todo-input" ref={$InputRef} placeholder="从这里开始添加..."></Input>
      <Button className="todo-btn" onClick={addTodo}>
        添加
      </Button>
      <Text className="text">--待办事项--</Text>
      <View className="todo-container">
        {todoList.map((todo, key) => {
          return (
            <View key={key} className="todo-Item">
              <View>
                {/* 具体事项 */}
                <View
                  className="left"
                  onClick={() => {
                    onChecked(key);
                  }}
                >
                  <Checkbox style={{marginRight:10}} checked={todo.checked} /> 
                  {todo.text}
                </View>
              </View>
              {/* 删除按钮 */}
              <Button className="todo-delete" onClick={handleDeleteClick}>删除</Button>
            </View>
          );
        })}
      </View>
      <View className="botBackground"></View>
    </View>
  );
};

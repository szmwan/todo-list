export default defineAppConfig({
  pages: [
    'pages/index/index',
    'pages/todoList/todoList'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
})
